# some sample Makefile

LIBNAME := foo
EXENAME := use-libfoo-so-make

all: $(EXENAME)

install: $(EXENAME)
	# the test
	install -D -m 755 $(EXENAME) $(DESTDIR)/$(EXENAME)

# for compilation outside of cooker mode 
# e.g. native Ubuntu or SDK:
$(EXENAME): $(libdir)/lib$(LIBNAME).so $(includedir)/$(LIBNAME).h
	$(CC) $(EXENAME).c -o $@ -L. -L$(libdir) -l$(LIBNAME) -I$(includedir)

# for compilation in cooker mode:
# .h and .so files are already where they should be
#$(EXENAME):
#	$(CC) $(CFLAGS) $(LDFLAGS) $(EXENAME).c -o $@ -l$(LIBNAME)

clean:
	$(RM) $(EXENAME) *.o *.so*
