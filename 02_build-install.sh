#!/bin/bash

set -x
rm -rf bin
mkdir bin
make -f Native.Makefile clean
DESTDIR=bin libdir=../lib-makefile-so/lib includedir=../lib-makefile-so/include make -f Native.Makefile install
set +x
